package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/admin/common")
@Api(tags = "通用的相关接口")
@Slf4j
public class CommonController {

    @Autowired
    AliOssUtil aliOssUtil;

    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public Result<String> upload(MultipartFile file){
        //1.接收参数
        log.info("接收文件参数：{}",file);
        //2.上传文件到alioss
        String url = null;
        try {
            //文件名覆盖问题
            //1.获取原始文件名
            String originalFilename = file.getOriginalFilename();
            //2.获取扩展名
            String extName = originalFilename.substring(originalFilename.lastIndexOf("."));
            //3.生成新的，唯一的文件名
            String newFileName = UUID.randomUUID().toString()+extName;

            url = aliOssUtil.upload(file.getBytes(), newFileName);
            //3.响应数据
            return Result.success(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.error(MessageConstant.UPLOAD_FAILED);
    }
}
